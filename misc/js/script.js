const data = {
  labels: [
    'molt deficient',
    'deficient',
    'acceptable',
    'bona',
    'molt bona'
  ],
  datasets: [{
    type: 'bar',
    label: 'Puntuació',
    data: [2, 3, 4, 2, 1],
    backgroundColor: [
      'rgba(255, 99, 132, 0.2)',
      'rgba(54, 162, 235, 0.2)',
      'rgba(255, 206, 86, 0.2)',
      'rgba(75, 192, 192, 0.2)',
      'rgba(153, 102, 255, 0.2)'
    ],
    borderColor: [
          'rgba(255, 99, 132, 1)',
          'rgba(54, 162, 235, 1)',
          'rgba(255, 206, 86, 1)',
          'rgba(75, 192, 192, 1)',
          'rgba(153, 102, 255, 1)'
    ],
    borderWidth: 1
  }, {
    type: 'line',
    label: 'Mitjana',
    data: [2.5,2.5,2.5,2.5,2.5],
    fill: false,
    borderColor: 'rgb(255, 0, 0)'
  }]
};
const config = {
  type: 'scatter',
  data: data,
  options: {
    scales: {
      yAxes: [{
          display: true,
          ticks: {
              beginAtZero: true,
          }
      }]
    },
  }
};
var myChart = new Chart(
  document.getElementById('myChart'),
  config
);