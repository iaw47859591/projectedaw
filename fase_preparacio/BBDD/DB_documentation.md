# DB Documentation  
De moment nomes hem creat una part de la BBDD per desenvolupar una part de la funcionalitat. La funcionalitat que permet a un estudiant respondre a una enquesta. De moment fen de costat la part referent als moduls de l'estudiant.  
  
# Requeriments

Ens demanen fer una BBDD d'Enquestes. Una enquesta està formada per plantilles (Per grup i per mòdul).  
Les plantilles poden estar formades per conjunts de preguntes, és a dir, un tema per aglutinar les preguntes. Cada conjunt i cada plantilla ha de tenir un titol.  
El conjunt de preguntes està format per preguntes. Aquestes han de tenir un text i un tipus (text o rang).  
Una pregunta pot no estar dintre d'un grup de preguntes.  
Les respostes que donarà l'alumne estarà vinculada a la pregunta i a la plantilla.  
Tenim diversos tipus d'usuaris (Alumnes, Professor i Cap de departament).  
L'alumne només pot responde enquestes una vegada per enquesta.  
El professor només pot visualitzar les respostes dels mòduls que imparteix.  
El cap de departament pot generar enquestes, veure totes les respostes i moderar les respostes(ocultar respostes a professors).  
Un alumne esta matriculat a un curs (2020/21).  
També s'ha matriculat a móduls. (M01, M02...)  
Un módul forma part d'un grup. (DAW, DAM...)  
Els grups pertanyen a un departament. (Informàtica, Arts, Mecànica...)  


## Diagrama E/R  
[Diagrama draw.io](https://drive.google.com/file/d/1SLUP3mwUvT_zcl6NnbGHmljVHH5ywOV3/view?usp=sharing)  
![Diagrama PNG](https://gitlab.com/iaw47859591/projectedaw/-/raw/master/fase_preparacio/diagrama/diagrama_er/DiagramaER4.0.png)  
![Diagrama PNG Ultima versió](img/diagramaER.4.0.png)  
[Diagrama Ultim draw.io](https://drive.google.com/file/d/1nR7kzocJTms_45UN_4XO41mxpKQr0Q3E/view?usp=sharing)  

**Rol**(<ins>idRol</ins>, tipoRol)  
**Usuario**(<ins>idUsuario</ins>, nombre, apellido1, apellido2)  
**RolxUsuario**(<ins>*idRol,idUsuario*</ins>)  
**Curso**(<ins>fechaCurso</ins>)  
**Modulo**(<ins>*idDepartamento,idGrupo*,idModulo</ins>, nombre)  
**UsuarioxCursoxModulo**(<ins>*FechaCurso,idUsuario,idDepartamento,idGrupo,idModulo*</ins>)  
**Grupo**(<ins>*idDepartamento*,idGrupo</ins>, nombre)  
**Departamento**(<ins>idDepartamento</ins>, nombre)  
**Enquesta**(<ins>idEnquesta</ins>, titulo, fechaInicio, fechaFin, activa)  
**UsuarioxEnquestaHace**(<ins>*idUsuario,idEnquesta*</ins>)  
**UsuarioxEnquestaResponde**(<ins>*idUsuario,idEnquesta*</ins>)  
**Plantilla**(<ins>idPlantilla</ins>, titulo, tipo)  
**EnquestaxPlantilla**(<ins>*idEnquesta,idPlantilla*</ins>)  
**ConjuntoPreguntas**(<ins>idConjunto</ins>, titulo, *idPlantilla*)  
**Pregunta**(<ins>idPregunta</ins>, textoPregunta, tipoRespuesta, *idConjunto*)  
**Respuesta**(<ins>idRespuesta</ins>, valorTex, valorRang, oculto, *idPregunta* *idModulo*)  

## DB diagram:  
![Database diagram](img/diagram.png "DB diagram")
  
## Table `\d`escription:  
![Database diagram](img/1.png "DB description 1")  
![Database diagram](img/2.png "DB description 2")  
![Database diagram](img/3.png "DB description 3")  
  
## DB seeder (sequelize)  
A partir de unes preguntes reals proporcionades per Jordi
```
'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert(
      'Enquestes',
      [
        {
          titol: 'enquesta fi de curs',
          datainici: new Date(Date.UTC(2021, 4, 1)),
          datafi: new Date(Date.UTC(2021, 5, 1)),
          activa: true,
        },
        {
          titol: 'Enquesta de 1r trimestre del curs 2020-21',
          datainici: new Date(Date.UTC(2020, 9, 1)),
          datafi: new Date(Date.UTC(2021, 10, 1)),
          activa: false,
        },
      ],
      {}
    );
    await queryInterface.bulkInsert(
      'Plantillas',
      [
        {
          titol: 'Enquesta General',
          tipus: 'general',
        },
        {
          titol: 'Enquesta de Modul',
          tipus: 'modul',
        },
      ],
      {}
    );
    await queryInterface.bulkInsert(
      'Enquestesxplantillas',
      [
        {
          idenquesta: 1,
          idplantilla: 1,
        },
        {
          idenquesta: 1,
          idplantilla: 2,
        },
        {
          idenquesta: 2,
          idplantilla: 1,
        },
        {
          idenquesta: 2,
          idplantilla: 2,
        },
      ],
      {}
    );
    await queryInterface.bulkInsert(
      'Conjuntpreguntes',
      [
        {
          titol: `En relació al centre, com d'acord estàs en aquests aspectes?`,
          idplantilla: 1,
        },
        {
          titol: `En relació a la tutoria, com d'acord estàs en aquests aspectes?`,
          idplantilla: 1,
        },
        {
          titol: `En relació a l'assignatura, com d'acord estàs en aquests aspectes?`,
          idplantilla: 2,
        },
        {
          titol: `En relació al professor, com d'acord estàs en aquests aspectes?`,
          idplantilla: 2,
        },
        {
          titol: `En relació al professor o a l'assignatura...Què és el que més t'agrada d'aquesta assignatura i d'aquest professor/a?`,
          idplantilla: 2,
        },
      ],
      {}
    );
    await queryInterface.bulkInsert(
      'Preguntes',
      [
        {
          text: `Les instal·lacions (aules, lavabos, etc.) són adequades`,
          tipus: 'radio',
          valrang: 5,
          idconjunt: 1,
        },
        {
          text: `Els ordinadors funcionen correctament?`,
          tipus: 'radio',
          valrang: 5,
          idconjunt: 1,
        },
        {
          text: `El/la tutor/a m'ha atès sempre que ho he necessitat`,
          tipus: 'radio',
          idconjunt: 2,
          valrang: 5,
        },
        {
          text: `La relació amb el/la tutor és cordial`,
          tipus: 'radio',
          idconjunt: 2,
          valrang: 5,
        },
        {
          text: `Les sessions de tutoria m'han sigut útils`,
          tipus: 'radio',
          idconjunt: 2,
          valrang: 5,
        },
        {
          text: `Què és el que més t'ha agradat de la tutoria?`,
          tipus: 'text',
          idconjunt: 2,
          valrang: null,
        },
        {
          text: `Què creus que es podria millorar?`,
          tipus: 'text',
          idconjunt: 2,
          valrang: null,
          
        },
        {
          text: `Els materials (apunts, exercicis, etc.) proporcionats són adequats, ordenats i clars`,
          tipus: 'radio',
          idconjunt: 3,
          valrang: 5,
          
        },
        {
          text: `La proporció entre hores de teoria i hores de pràctiques és l'adequada`,
          tipus: 'radio',
          idconjunt: 3,
          valrang: 5,
          
        },
        {
          text: `L'esforç necessari per aprovar és raonable`,
          tipus: 'radio',
          idconjunt: 3,
          valrang: 5,
          
        },
        {
          text: `S'explica amb claredat`,
          tipus: 'radio',
          idconjunt: 4,
          valrang: 5,
          
        },
        {
          text: `Domina i coneix el tema de l'assignatura`,
          tipus: 'radio',
          idconjunt: 4,
          valrang: 5,
          
        },
        {
          text: `Compleix amb l'horari de les classes`,
          tipus: 'radio',
          idconjunt: 4,
          valrang: 5,
          
        },
        {
          text: `Què és el que més t'agrada d'aquesta assignatura i d'aquest professor/a? `,
          tipus: 'text',
          idconjunt: 5,
          valrang: null,
          
        },
        {
          text: `Què creus que es podria millorar?`,
          tipus: 'text',
          idconjunt: 5,
          valrang: null,
          
        },
      ],
      {}
    );
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropAllTables();
  }
};

```
