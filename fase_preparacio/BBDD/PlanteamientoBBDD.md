# Diagrama E/R  
[Diagrama draw.io](https://drive.google.com/file/d/1SLUP3mwUvT_zcl6NnbGHmljVHH5ywOV3/view?usp=sharing)  
![Diagrama PNG](https://gitlab.com/iaw47859591/projectedaw/-/raw/master/fase_preparacio/diagrama/diagrama_er/DiagramaER4.0.png)


**Rol**(<ins>idRol</ins>, tipoRol)  
**Usuario**(<ins>idUsuario</ins>, nombre, apellido1, apellido2)  
**RolxUsuario**(<ins>*idRol,idUsuario*</ins>)  
**Curso**(<ins>fechaCurso</ins>)  
**Modulo**(<ins>*idDepartamento,idGrupo*,idModulo</ins>, nombre)  
**UsuarioxCursoxModulo**(<ins>*FechaCurso,idUsuario,idDepartamento,idGrupo,idModulo*</ins>)  
**Grupo**(<ins>*idDepartamento*,idGrupo</ins>, nombre)  
**Departamento**(<ins>idDepartamento</ins>, nombre)  
**Enquesta**(<ins>idEnquesta</ins>, titulo, fechaInicio, fechaFin, activa)  
**UsuarioxEnquestaHace**(<ins>*idUsuario,idEnquesta*</ins>)  
**UsuarioxEnquestaResponde**(<ins>*idUsuario,idEnquesta*</ins>)  
**Plantilla**(<ins>idPlantilla</ins>, titulo, tipo)  
**EnquestaxPlantilla**(<ins>*idEnquesta,idPlantilla*</ins>)  
**ConjuntoPreguntas**(<ins>idConjunto</ins>, titulo, *idPlantilla*)  
**Pregunta**(<ins>idPregunta</ins>, textoPregunta, tipoRespuesta, *idConjunto*)  
**Respuesta**(<ins>idRespuesta</ins>, valorTex, valorRang, oculto, *idPregunta* *idModulo*)  




# Perfils

Por cada usuario que se logee en la pagina de encuesta, nos interesa saber:

- codiUsuari
- nom
- cognom1
- cognom2
- moduls
- curs
- grup
- departament
- rol (membre de direccio, cap de departament, tutor, profesor,  estudiante)

