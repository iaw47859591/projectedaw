# Requeriments

Ens demanen fer una BBDD d'Enquestes. Una enquesta està formada per plantilles (Per grup i per mòdul).
Les plantilles poden estar formades per conjunts de preguntes, és a dir, un tema per aglutinar les preguntes. Cada conjunt i cada plantilla ha de tenir un titol.
El conjunt de preguntes està format per preguntes. Aquestes han de tenir un text i un tipus (text o rang).
Una pregunta pot no estar dintre d'un grup de preguntes.
Les respostes que donarà l'alumne estarà vinculada a la pregunta i a la plantilla.
Tenim diversos tipus d'usuaris (Alumnes, Professor i Cap de departament).
L'alumne només pot responde enquestes una vegada per enquesta.
El professor només pot visualitzar les respostes dels mòduls que imparteix.
El cap de departament pot generar enquestes, veure totes les respostes i moderar les respostes(ocultar respostes a professors).
Un alumne esta matriculat a un curs (2020/21).
També s'ha matriculat a móduls. (M01, M02...)
Un módul forma part d'un grup. (DAW, DAM...)
Els grups pertanyen a un departament. (Informàtica, Arts, Mecànica...)

## Preguntes del mail
2. Entre usuari i enquesta hi ha dues relacions, les podrieu explicar (a requeriments.md).
Un cap de departament pot crear enquestes. Un alumne pot responde. Un professor pot veure les respostes als mòduls que imparteix, hem de crear una altra relació?

3. Els codis de grup són únics (1HIAW), amb el què no serà febre per Id amb Dept.
Sí, no serà feble per ID.

4. Les enquestes entenc que es fan per mòdul però no per UF, oi?
Exclusivament per mòdul.

5. La relació enquesta i plantilla la podeu explicar? (requriments.md)
Explicat als requeriments.

6. Podeu il.lustrar amb algun exemple per poder entendre bé el cas (Plantilla, conjunt_preguntes, preguntes i respostes)
Una plantilla de mòdul es repetirà (a l'hora de mostrar-li a l'estudiant) tantes vegades com mòduls estigui cursant l'alumne.
El conjunt de preguntes preten aglutinar les preguntes per temàtica. 
Enquesta -> Plantilla -> Conjunt de preguntes -> Preguntes 

7. Què  és el campo oculto? Això més que el nom d'un camp, sembla una propietat del camp.
Forma part de les potestats del moderador (cap de departament). Quan una resposta està oculta el professor no podrà visualitzar-la.
