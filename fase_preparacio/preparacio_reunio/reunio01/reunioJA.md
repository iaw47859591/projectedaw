# Reunio amb J.A.

## Preguntes

## Arquitectura
- Teniu documentació o algun disseny de l'arquitectura de l'aplicació web de l'Escola del treball?

### Tecnologies
- Quines tecnologies useu a l'Escola del Treball?
- Tindrieu inconvenients si usem Laravel/Nodejs i Vue?

## Base de Dades
- Ens podries fer una breu explicació de com useu la BBDD?
- Teniu documentació o algun disseny Entitat/Relació de la vostra BBDD?
- Podriem importar la BBDD per poder treballar amb inputs reals? Com podem fer-ho?
    - Si no podem replicar la base de dades amb entitats dummies.
- Trobarem problemes amb usuaris antics? Com ho gestioneu?
- Creiem que haurem de crear noves taules per emmagatzemar preguntes i respostes ho veus factible? Com realitzarem la pujada a producció?
- El nostre plantejament per a la base de dades es la següent:
    - Enviar credencials contra el servidor.
    - Crear un sessió.
    - Guardar credencials d'alumnes per verificar qui a completat quina enquesta.
    - Guardar les respostes anonimament a una altra taula.

- La base de dades ens retornarà (en cas de professors) si és cap de departament o membre de direcció? Quins moduls impoarteix...?
- La base de dades ens retornarà (en cas de alumnes) quines assignatures rep?
- Els mòduls són únics? (Clau propia que identifica curs, any, ...)

## Login
- Com gestioneu els logins de l'aplicació?
- Si un usuari ja està loguejat a l'Escola del Treball, com podem mantenir el login a la nostra app?

