# Investigació

Per a crear una aplicació forta, accessible i amb bona usabilitat hem pensat que una bona forma de començar és fere una recerca d'altres aplicacions amb funcions similars.

## Pàgines visitades
[Google Forms](https://docs.google.com/forms)  
[Survey legend](https://www.surveylegend.com/)  
[Doodle](https://doodle.com/)  

## Items

### Opcions de resposta (custom).
Els formularis visitats tenen tipus d'opcions per a les respostes.  
- Resposta breu.
- Paràgraf
-  Caselles de selecció *radiobuttons*.
    - Multiples columnes Ex: si/no 1-2-3-4-5
- Selecció multiple*checkbox*.
    - Graella de multiple selecció.
- Deplegable *select/option*.
- Data *date*.
- Hora *time*.

### Opcions de preguntes
- Obligatorietat per respondre les preguntes.
- Descripció de la pregunta.
- Copiar la pregunta.
- Validació de respostes.
- Eliminar pregunta.

### Importacions
Alguns formularis permeten importar:  
- Preguntes d'altres formularis.
- Imatges.
- Videos.

### Visualització de les respostes
- Visualització general, per pregunta, o individual.
- A les opcions de radiobuttons i checkbox es generen gràfics amb les respostes.
- Si hi ha contingut explicit o malsonant se li farà constar amb un warning al cap de departament.

### Limitacions
- Limitar els enquestats a una sola resposta.
- Limitar les respostes a la primera opció escollida.

### Altres
- Opció d'esborra la resposta als radiobuttons.
- Permetre fer un full de càlcul amb les respotes. (Podriem fer preguntar pel format que més interessi al centre).
