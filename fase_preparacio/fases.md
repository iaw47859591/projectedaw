# Fases del projecte

Com que el projecte de les enquestes és molt ampli i s'hi poden afegir multiples funcionaliats creiem convenient dividir aquest en fases.
Aquestes s'ordenaràn de més necessaries a menys.

## Fase 1
- Implementar la BBDD.
- Rols d'estudiant, professor i cap de departament.
- Login per rol.
- Enquesta tindrà data d'inici i final per contestar-la.
- Es pot crear més d'una enquesta (només una serà activa).
- Enquesta dividida per plantilles(Grup i Mòdul).
- Plantilla de mòdul seràn les mateixes per a tots els mòduls de l'estudiant.
- Cap de departament modifica enquesta.
- Cap de departament visualitzar totes les respostes.
- Cap de departament oculta respostes.
- Professor visualitzar respostes dels mòduls que imparetix (només les no ocultes).
- Estudiant respon, només una vegada, l'enquesta.
- Estudiant visualitzar tantes enquestes(plantilla) de mòduls com mòduls estigui matriculat.
- Preguntes només tindran dos opcions de resposta (Text i Rang).

## Fase 2
- Més d'una enquesta activa.
- Estudiant podrà seleccionar quina enquesta respondre.
- Rol tutor
- Cap de departament visualitza les preguntes abans que el rols menors?
- Tutor pot visualitzar respostes de grup.
- Professor pot afegir preguntes a enquestes dels seus mòduls (només visibles als alumnes dels mòduls que imparteix el professor).
- Preguntes tindran més opcions de resposta.
- Comparar amb altres enquestes.

## Fase 3
- Rol direcció.
- Direcció pot afegir crear enquestes de grup i mòduls.
- Direcció pot llegir totes les enquestes.
- Cap de departament pot afegir preguntes a les enquestes de direcció (només visibles als alumnes del departament).
- Cap de depeartament pot crear enquestes per al seu departament.
- Cap de departament només pot llegir respostes del seu departament.
- Tutor pot afegir preguntes a l'enquesta de grup.
- Professor pot afegir preungtes a l'enquesta de mòdul.
- Exportar les respostes a altres formats.

## Fase 4
- Tots els rols, menys estudiants, poden crear enquestes.
