import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import VuePaginate from 'vue-paginate'
import axios from 'axios'
import VueAxios from 'vue-axios'
Vue.use(VuePaginate,VueAxios, axios)

axios.defaults.baseURL = 'http://localhost:3000/';


Vue.config.productionTip = false;

new Vue({
  router,
  render: (h) => h(App),
}).$mount("#app");
