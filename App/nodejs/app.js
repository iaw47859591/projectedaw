require("dotenv").config();
var port = process.env.SERVER_PORT;
var express = require("express");
var app = express();
var path = require("path");
//Layouts
const expressLayouts = require('express-ejs-layouts');
// Cors
const cors = require('cors');
// postgres
const { Pool } = require('pg')
// file reader
var fs = require('fs');
// sequelize connection

const { connection } = require('./app/database/db');
//Motor plantillas EJS y Layouts
app.set('view engine', 'ejs');
app.set('views', __dirname + '/views');
app.use(expressLayouts);


// cors
app.use(cors());

//Puerto
var server = require("http").createServer(app).listen(port, ()=> {
    console.log("Puerto conectado: " + port);
    connection.sync({ force: true}).then( () => {
        console.log('Conectado a la BBDD');
    })
})
//Rutas
var rutas = require("./app/routes/index");
app.use('/', rutas);

//Error 404
app.use((req, res, next) => {
    res.status(404).sendFile(__dirname + '/views/404.html');
});

