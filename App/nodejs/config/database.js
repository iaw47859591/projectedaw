
require("dotenv").config();
module.exports = {
  username: process.env.POSTGRES_USER || "devroot",
  password: process.env.POSTGRES_PASSWORD || "devroot",
  database: process.env.POSTGRES_DB || "enquestes",
  host: process.env.POSTGRES_URI || "postgres",
  dialect: "postgres" || "postgres",
  define: {
    timestamps: false,
  }
}