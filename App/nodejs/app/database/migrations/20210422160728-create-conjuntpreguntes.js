'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Conjuntpreguntes', {
      idconjunt: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      titol: {
        type: Sequelize.STRING
      },
      idplantilla: {
        allowNull: false,
        type: Sequelize.INTEGER,
        references: {
          model: 'Plantillas',
          key: 'idplantilla'
        }
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Conjuntpreguntes');
  }
}; 