'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Plantillas', {
      idplantilla: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      titol: {
        type: Sequelize.STRING
      },
      tipus: {
        type: Sequelize.DataTypes.ENUM('general','modul')
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Plantillas');
  }
}; 