'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Respostes', {
      idresposta: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      valtext: {
        type: Sequelize.STRING
      },
      valrang: {
        type: Sequelize.INTEGER
      },
      tipus: {
        type: Sequelize.DataTypes.ENUM('text','radio')
      },
      ocult: {
        type: Sequelize.BOOLEAN
      },
      idpregunta: {
        allowNull: false,
        type: Sequelize.INTEGER,
        references: {
          model: 'Preguntes',
          key: 'idpregunta'
        }
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Respostes');
  }
}; 