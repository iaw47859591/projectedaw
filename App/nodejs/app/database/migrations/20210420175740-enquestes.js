'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /*
    // enquestes
    await queryInterface.createTable('enquestes', {
    idenquesta: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER
    },
    titol: {
      type: Sequelize.STRING
    },
    datainici: {
      allowNull: false,
      type: Sequelize.DATE
    },
    datafi: {
      allowNull: false,
      type: Sequelize.DATE
    },
    activa: {
      allowNull: false,
      type: Sequelize.BOOLEAN
    },
    created_at: {
      allowNull: false,
      type: Sequelize.DATE
    },
    updated_at: {
      allowNull: false,
      type: Sequelize.DATE
    }
  });
    // enquestes
    await queryInterface.createTable('plantilla', {
      idplantilla: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      titol: {
        type: Sequelize.STRING
      },
      tipus: {
        type: Sequelize.DataTypes.ENUM('general','modul')
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
    // enquestesXplantilla
    await queryInterface.createTable('enquestesxplantilla', {
      idenquesta: {
        allowNull: false,
        type: Sequelize.INTEGER,
        references: {
          model: 'enquestes',
          key: 'idenquesta'
        }
      },
      idplantilla: {
        allowNull: false,
        type: Sequelize.INTEGER,
        references: {
          model: 'plantilla',
          key: 'idplantilla'
        }
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      }
    })
    // adding primary key constraint
    .then(() => {
      return queryInterface.sequelize.query('ALTER TABLE "enquestesxplantilla" ADD CONSTRAINT "enquestaxplantilla_pk" PRIMARY KEY ("idenquesta", "idplantilla")');
    });
    // conjuntpreguntes
    await queryInterface.createTable('conjuntpreguntes', {
      idconjunt: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      titol: {
        type: Sequelize.STRING
      },
      idplantilla: {
        allowNull: false,
        type: Sequelize.INTEGER,
        references: {
          model: 'plantilla',
          key: 'idplantilla'
        }
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
    // pregunta
    await queryInterface.createTable('preguntes', {
      idpregunta: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      text: {
        type: Sequelize.STRING
      },
      tipus: {
        type: Sequelize.DataTypes.ENUM('text','radio')
      },
      valrang: {
        type: Sequelize.INTEGER
      },
      idconjunt: {
        allowNull: true,
        type: Sequelize.INTEGER,
        references: {
          model: 'conjuntpreguntes',
          key: 'idconjunt'
        }
      },
      idplantilla: {
        allowNull: true,
        type: Sequelize.INTEGER,
        references: {
          model: 'plantilla',
          key: 'idplantilla'
        }
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
    // resposta
    await queryInterface.createTable('respostes', {
      idresposta: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      valtext: {
        type: Sequelize.STRING
      },
      valrang: {
        type: Sequelize.INTEGER
      },
      tipus: {
        type: Sequelize.DataTypes.ENUM('text','radio')
      },
      ocult: {
        type: Sequelize.BOOLEAN
      },
      idpregunta: {
        allowNull: false,
        type: Sequelize.INTEGER,
        references: {
          model: 'preguntes',
          key: 'idpregunta'
        }
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
    */
  },

  down: async (queryInterface, Sequelize) => {
    /*
    await queryInterface.dropAllTables();
    */
  }
};
