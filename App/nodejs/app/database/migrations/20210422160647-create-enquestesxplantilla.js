'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Enquestesxplantillas', {
      idenquesta: {
        allowNull: false,
        type: Sequelize.INTEGER,
        references: {
          model: 'Enquestes',
          key: 'idenquesta'
        }
      },
      idplantilla: {
        allowNull: false,
        type: Sequelize.INTEGER,
        references: {
          model: 'Plantillas',
          key: 'idplantilla'
        }
      }
    })
    // adding primary key constraint
    .then(() => {
      return queryInterface.sequelize.query('ALTER TABLE "Enquestesxplantillas" ADD CONSTRAINT "enquestesxplantillas_pk" PRIMARY KEY ("idenquesta", "idplantilla")');
    })
    
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Enquestesxplantillas');
  }
}; 