'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Preguntes', {
      idpregunta: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      text: {
        type: Sequelize.STRING
      },
      tipus: {
        type: Sequelize.STRING
      },
      valrang: {
        type: Sequelize.INTEGER
      },
      idconjunt: {
        allowNull: true,
        type: Sequelize.INTEGER,
        references: {
          model: 'Conjuntpreguntes',
          key: 'idconjunt'
        }
      },
      idplantilla: {
        allowNull: true,
        type: Sequelize.INTEGER,
        references: {
          model: 'Plantillas',
          key: 'idplantilla'
        }
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Preguntes');
  }
}; 