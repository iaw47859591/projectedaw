'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Enquestes', {
      idenquesta: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      titol: {
        type: Sequelize.STRING
      },
      datainici: {
        type: Sequelize.DATE
      },
      datafi: {
        type: Sequelize.DATE
      },
      activa: {
        type: Sequelize.BOOLEAN
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Enquestes');
  }
}; 