'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /*
    await queryInterface.bulkInsert(
      'enquestes',
      [
        {
          titol: 'enquesta fi de curs',
          datainici: new Date(Date.UTC(2021, 4, 1)),
          datafi: new Date(Date.UTC(2021, 5, 1)),
          activa: true,
          created_at: new Date(),
          updated_at: new Date(),
        },
        {
          titol: 'Enquesta de 1r trimestre del curs 2020-21',
          datainici: new Date(Date.UTC(2020, 9, 1)),
          datafi: new Date(Date.UTC(2021, 10, 1)),
          activa: false,
          created_at: new Date(),
          updated_at: new Date(),
        },
      ],
      {}
    );
    await queryInterface.bulkInsert(
      'plantilla',
      [
        {
          titol: 'Enquesta General',
          tipus: 'general',
          created_at: new Date(),
          updated_at: new Date(),
        },
        {
          titol: 'Enquesta de Modul',
          tipus: 'modul',
          created_at: new Date(),
          updated_at: new Date(),
        },
      ],
      {}
    );
    await queryInterface.bulkInsert(
      'enquestesxplantilla',
      [
        {
          idenquesta: 1,
          idplantilla: 1,
          created_at: new Date(),
          updated_at: new Date(),
        },
        {
          idenquesta: 1,
          idplantilla: 2,
          created_at: new Date(),
          updated_at: new Date(),
        },
        {
          idenquesta: 2,
          idplantilla: 1,
          created_at: new Date(),
          updated_at: new Date(),
        },
        {
          idenquesta: 2,
          idplantilla: 2,
          created_at: new Date(),
          updated_at: new Date(),
        },
      ],
      {}
    );
    await queryInterface.bulkInsert(
      'conjuntpreguntes',
      [
        {
          titol: `En relació al centre, com d'acord estàs en aquests aspectes?`,
          idplantilla: 1,
          created_at: new Date(),
          updated_at: new Date(),
        },
        {
          titol: `En relació a la tutoria, com d'acord estàs en aquests aspectes?`,
          idplantilla: 1,
          created_at: new Date(),
          updated_at: new Date(),
        },
        {
          titol: `En relació a l'assignatura, com d'acord estàs en aquests aspectes?`,
          idplantilla: 2,
          created_at: new Date(),
          updated_at: new Date(),
        },
        {
          titol: `En relació al professor, com d'acord estàs en aquests aspectes?`,
          idplantilla: 2,
          created_at: new Date(),
          updated_at: new Date(),
        },
        {
          titol: `En relació al professor o a l'assignatura...Què és el que més t'agrada d'aquesta assignatura i d'aquest professor/a?`,
          idplantilla: 2,
          created_at: new Date(),
          updated_at: new Date(),
        },
      ],
      {}
    );
    await queryInterface.bulkInsert(
      'preguntes',
      [
        {
          text: `Les instal·lacions (aules, lavabos, etc.) són adequades`,
          tipus: 'radio',
          valrang: 5,
          idconjunt: 1,
          created_at: new Date(),
          updated_at: new Date(),
        },
        {
          text: `Els ordinadors funcionen correctament?`,
          tipus: 'radio',
          valrang: 5,
          idconjunt: 1,
          created_at: new Date(),
          updated_at: new Date(),
        },
        {
          text: `El/la tutor/a m'ha atès sempre que ho he necessitat`,
          tipus: 'radio',
          idconjunt: 2,
          valrang: 5,
          created_at: new Date(),
          updated_at: new Date(),
        },
        {
          text: `La relació amb el/la tutor és cordial`,
          tipus: 'radio',
          idconjunt: 2,
          valrang: 5,
          created_at: new Date(),
          updated_at: new Date(),
        },
        {
          text: `Les sessions de tutoria m'han sigut útils`,
          tipus: 'radio',
          idconjunt: 2,
          valrang: 5,
          created_at: new Date(),
          updated_at: new Date(),
        },
        {
          text: `Què és el que més t'ha agradat de la tutoria?`,
          tipus: 'text',
          idconjunt: 2,
          valrang: null,
          created_at: new Date(),
          updated_at: new Date(),
        },
        {
          text: `Què creus que es podria millorar?`,
          tipus: 'text',
          idconjunt: 2,
          valrang: null,
          created_at: new Date(),
          updated_at: new Date(),
        },
        {
          text: `Els materials (apunts, exercicis, etc.) proporcionats són adequats, ordenats i clars`,
          tipus: 'radio',
          idconjunt: 3,
          valrang: 5,
          created_at: new Date(),
          updated_at: new Date(),
        },
        {
          text: `La proporció entre hores de teoria i hores de pràctiques és l'adequada`,
          tipus: 'radio',
          idconjunt: 3,
          valrang: 5,
          created_at: new Date(),
          updated_at: new Date(),
        },
        {
          text: `L'esforç necessari per aprovar és raonable`,
          tipus: 'radio',
          idconjunt: 3,
          valrang: 5,
          created_at: new Date(),
          updated_at: new Date(),
        },
        {
          text: `S'explica amb claredat`,
          tipus: 'radio',
          idconjunt: 4,
          valrang: 5,
          created_at: new Date(),
          updated_at: new Date(),
        },
        {
          text: `Domina i coneix el tema de l'assignatura`,
          tipus: 'radio',
          idconjunt: 4,
          valrang: 5,
          created_at: new Date(),
          updated_at: new Date(),
        },
        {
          text: `Compleix amb l'horari de les classes`,
          tipus: 'radio',
          idconjunt: 4,
          valrang: 5,
          created_at: new Date(),
          updated_at: new Date(),
        },
        {
          text: `Què és el que més t'agrada d'aquesta assignatura i d'aquest professor/a? `,
          tipus: 'text',
          idconjunt: 5,
          valrang: null,
          created_at: new Date(),
          updated_at: new Date(),
        },
        {
          text: `Què creus que es podria millorar?`,
          tipus: 'text',
          idconjunt: 5,
          valrang: null,
          created_at: new Date(),
          updated_at: new Date(),
        },
      ],
      {}
    );
    */
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropAllTables();
  }
};
