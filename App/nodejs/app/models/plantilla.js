'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Plantilla extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  Plantilla.init({
    idplantilla: DataTypes.INTEGER,
    titol: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Plantilla',
  });
  return Plantilla;
};