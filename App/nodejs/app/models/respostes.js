'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Respostes extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  Respostes.init({
    idresposta: DataTypes.INTEGER,
    valtext: DataTypes.STRING,
    valrang: DataTypes.INTEGER,
    tipus: DataTypes.STRING,
    ocult: DataTypes.BOOLEAN,
    idpregunta: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Respostes',
  });
  return Respostes;
};