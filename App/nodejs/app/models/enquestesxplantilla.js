'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Enquestesxplantilla extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  Enquestesxplantilla.init({
    idenquesta: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    },
    idplantilla: {
      type: DataTypes.INTEGER,
      primaryKey: true,
    }
  }, {
    sequelize,
    modelName: 'Enquestesxplantilla',
  });
  return Enquestesxplantilla;
};