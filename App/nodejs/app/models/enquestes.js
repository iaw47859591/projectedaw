'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Enquestes extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  Enquestes.init({
    idenquesta: DataTypes.INTEGER,
    titol: DataTypes.STRING,
    datainici: DataTypes.DATE,
    datafi: DataTypes.DATE,
    activa: DataTypes.BOOLEAN
  }, {
    sequelize,
    modelName: 'Enquestes'
  });
  return Enquestes;
};