'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Conjuntpreguntes extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  Conjuntpreguntes.init({
    idconjunt: DataTypes.INTEGER,
    titol: DataTypes.STRING,
    idplantilla: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Conjuntpreguntes',
  });
  return Conjuntpreguntes;
};