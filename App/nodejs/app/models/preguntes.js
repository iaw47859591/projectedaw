'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Preguntes extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  Preguntes.init({
    idpregunta: DataTypes.INTEGER,
    text: DataTypes.STRING,
    tipus: DataTypes.STRING,
    valrang: DataTypes.INTEGER,
    idconjunt: DataTypes.INTEGER,
    idplantilla: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Preguntes',
  });
  return Preguntes;
};