var path = require('path');
var express = require('express');
var router = express.Router();

var ctrlDir = '/app/app/controllers'
var vaDir = require(path.join(ctrlDir, 'vistaalumne'));

router.get('/', (req, res) => {
   res.send('ok');
});

router.get('/responderencuesta', vaDir.enviarPreguntes);

router.post('/enviarencuesta', vaDir.guardarPreguntes);

module.exports = router;