exports.enviarPreguntes = (req, res, next) => {
    // connectar amb bbdd

   // carregar dades necessàries a json
   const dummy = {
      id: 1,
      titulo: "titulo enquesta",
      plantillas: [{
         id: 1,
         titulo: "Enquesta general",
         tipo: "general",
         conjunto: [{
            titulo: "string",
            preguntas: [{
               id: 1,
               texto: "string",
               tipo: "input"
            }, 
            {
               id: 2,
               texto: "string",
               tipo: "radio",
               rango: 5               
            }]
         },
         {
            titulo: "string",
            preguntas: [{
               id: 3,
               texto: "string?",
               tipo: "input"
            }]
         }]
      },
      {
         id: 2,
         titulo: "Enquesta modulo",
         tipo: "modulo",
         conjunto: [{
            titulo: "string",
            preguntas: [{
               id: 4,
               texto: "string",
               tipo: "rango"
            }]
         },
         {
            titulo: "string",
            preguntas: [{
               id: 5,
               texto: "string",
               tipo: "input"
            }]
         }]
      }]
   }
   // respondre amb el json
   res.json(dummy);
}

exports.guardarPreguntes = (req, res) => {
    const b = req.body;
    if (true) {
       res.send("Respostes pujades correctament.")
    } else {
       res.send("Respostes no pujades correctament.")
    }
 };